package wrappers;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import utils.parseHTMLFile;
import features.AoANormFeaturesExtractor;
import features.CelexMorphFeaturesExtractor;
import features.CelexSynFeaturesExtractor;
import features.LexSynWnFeaturesExtractor;;

/**
 * @author svajjala
 * Take a HTML file, Plain Text or a Plain Text file and extract all the readability/Complexity features for this data.
 */
public class ExtractFeaturesForGivenText {

	/**
	 * @param args[0], args[1].
	 * if Args[0]=1, process the String as a HTML file path.
	 * if args[0]=2, process the String as a plaintext file path.
	 * if args[0]=3, process the String as the text itself! 
	 * if args[0]=4. process the input string as a URL path.
	 * May be there should be options for batch mode..like give a directory path containing plaintext files...and get as output an array of readinglevels for those texts.
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
	
	/*	String finalheader = fileid + syntacticComplexityFeaturesHeader + celexMorphFeaturesHeader+ celexSynFeaturesHeader
							+ aoaFeaturesHeader+ "class"; 
		*/
	
		//initialize classes for lex, syn
		LexSynWnFeaturesExtractor ptfe = new LexSynWnFeaturesExtractor();
		
		//Initialize classes for Celex Extraction:
		CelexMorphFeaturesExtractor.initialize();
		CelexSynFeaturesExtractor.initialize();
		AoANormFeaturesExtractor.initialize(); 
		
		String textcontent = ""; //Variable that stores the content of the file.
			try{			
			//	filecontent = text;
			    while (!textcontent.equals("exit")) {
			    	System.out.println("*****************" + "\n" + "Please enter a new sentence: ");
					BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
					textcontent = bufferRead.readLine();
			    	String featurestring = "";
					featurestring = extractLexSyn(textcontent,ptfe)+","+extractCelexFeatures(textcontent)+","+extractAoAFeatures(textcontent);	
					System.out.println("Feature string: " + featurestring);
					System.out.println("Reading level for this text is" + getReadingLevel(featurestring));
			    }
				
				
			//	System.out.println("Reading level for this document is" + getReadingLevel(featurestring));
			//	System.out.println("This is suitable for learners of age group: " + mapReadingLeveltoAgeGroup(getReadingLevel(featurestring)));
			}
			catch(Exception e1)
			{
				System.out.println("Error trying to process file: " + " " + e1.toString());
				//continue;
			}
	
	}
	
	/**
	 * Supposed to be feature string creator... but for now, this happens in the main method itself.
	 * @param filecontent
	 * @return
	 * @throws Exception
	 */
	private static String getFeatureString(String filecontent) throws Exception
	{
		
		return null;
	}
	
	/**
	 * Extracts the contents of a text file.
	 * @param filepath - path containing the file
	 * @return the text in this file
	 * @throws Exception
	 */
	private static String getTheFileContent(String filepath) throws Exception
	{
		String result = "";
		BufferedReader br = new BufferedReader(new FileReader(filepath));
		String dummy = "";
		while((dummy = br.readLine())!=null)
		{
			result += dummy+ "\n";
		}
		br.close();
		return result;
	}
	
	/**
	 * Returns these features in this order: "sent,senlen,nouns,propernouns,pronouns,conj,adj,ver,numnp,numvp," + 
	    "numpp,numsbar,avgnpsize,avgvpsize,avgppsize,avgparsetreeheight,numclauses,mlc,numtunits,mlt,cnperc,cnpert,depcperc,depcpert,coordperc,coordpert,"+
	     "vppert,lexicals,advvar,adjvar,modvar,"+"nounvar,verbvar,depwords,distheadword,numconstituents,numsubtrees,distsemhead,numwh,rrc,interj,conjp,adverbs,modals,numsenses,commas,perpro,whpro,"+
	    "numfuncwords,numdet,numvb,numvbd,numvbg,numvbn,numvbp,unparsable,ttr,cttr,bilog,uber,";
	 * @param text - text for which features are needed.
	 * @param ptfe - the object for that feature extractor class which gets Lex, Syn and WN features.
	 * @return feature string.
	 */
	private static String extractLexSyn(String text, LexSynWnFeaturesExtractor ptfe)
	{
		String result = ptfe.extractParseFeatures(text);
		return result; //Returns a string containing all the lexical and syntactic features.
	}
		
	/***
	 * Returns Morph Features in this order: "numsentences,percentwordsconsidered,percentwordsnotconsidered,morphcomplex,morphmonomorphic,morphconversions,morphcontractions," +
						 "morphirrelevant,morphobscure,morphmayincluderoot,morphundetermined,foreignwords,moreanalyses,nvaffcomp,der,comp,dercomp,"+
						"intrans,trans,transintrans,unmarkedtrans,sastem,saaffix,sasanda,saflex,sasandflex,alloblend,alloclip,alloderiv,alloflex,alloconv,"+
						"subst,opacity,transderhash,transderadd,transderremov,infix,reversal,";
     	 * and Syn features in this order: String celexSynFeaturesHeader = "numsentences2,percentwordsconsidered2,percentwordsnotconsidered2,numnouns,numpropernouns,numadj,numverb," +
						 "numarticle,numpron,numadv,numprep,numconj,numinterj,numcountablen,numuncountablen,numsingularn,numpluraln,"+
						 "numgroupcount,numgroupuncount,numattrn,numpostposN,numvocN,numExprN,transV,transcompV,intransV,ditransV,linkingV,"+
						 "phrasalV,prepV,phrprepV,exprV,ordAdj,attrAdj,predAdj,postposAdj,exprAdj,ordAdv,predAdv,postposAdv,combAdv,ExpAdv,"+
						 "perPro,demonPro,possPro,reflPro,whPro,detPro,pronPro,expPro,coordConj,subordConj,";
	 * @param text - text for which features are extracted
	 * @return - feature string.
	 * @throws Exception
	 */
	private static String extractCelexFeatures(String text) throws Exception
	{
		String result = CelexMorphFeaturesExtractor.getMorphFeatures(text, "");
		result += "," + CelexSynFeaturesExtractor.getSynFeatures(text, "");
		return result;
	}
	
	/***
	 * Returns AoA and MRC Features in this order: String aoaFeaturesHeader = "AoA_Kup,AoA_kup_Lem,AoA_Bird_Lem,AoA_Bristol_Lem,AoA_Cort_Lem," + 
						 "familiarity,concreteness,imagery,colMeaningful,pavioMeaning,AoA_MRC,";
	 * @param text - for which features are needed
	 * @return feature string.
	 * @throws Exception
	 */
	private static String extractAoAFeatures(String text) throws Exception
	{
		String result = AoANormFeaturesExtractor.getAoAMRCFeatures(text);
		return result;
	}
	
	/**
	 * Prints a descriptive output about the text.
	 * @param featurestring - The feature string generated by running other classes. 
	 * @return -none. This method just prints a descriptive output listing whatever features we want to be listed here.
	 * @throws Exception
	 */
	private static void getDescriptiveOutput(String featurestring) throws Exception
	{
		//Use the buildFeaturesMap() method to get the features into a HashMap. Then, pick whatever we eventually will present in the output and present them.
	}
	
	/**
	 * Returns the reading level of a text based on its feature string, using the regression formula in about/ folder.
	 * @param featurestring
	 * @return A double value for reading level
	 * @throws Exception
	 */
	private static double getReadingLevel(String featurestring) throws Exception
	{
		double readingLevel = 0.0;
		HashMap<String, Double> featuresmap = buildFeaturesMap(featurestring);
		
		readingLevel =  (0.0105*featuresmap.get("sent"))+(0.0693*featuresmap.get("senlen"))+(-1.9797*featuresmap.get("nouns"))+(0.7364*featuresmap.get("propernouns"))
						+(-0.9419*featuresmap.get("pronouns"))+(3.2162*featuresmap.get("conj"))+(6.052*featuresmap.get("adj"))+(-5.3651*featuresmap.get("ver"))
						+(0.0433*featuresmap.get("numnp"))+(-0.1545*featuresmap.get("numvp"))+(0.1642*featuresmap.get("numpp"))+(0.3194*featuresmap.get("numsbar"))
						+(-0.3981*featuresmap.get("avgppsize"))+(-0.1057*featuresmap.get("avgparsetreeheight"))+(0.1178*featuresmap.get("numclauses"))
						+(-0.0117*featuresmap.get("mlc"))+(-0.0775*featuresmap.get("numtunits"))+(-0.196*featuresmap.get("mlt"))+(-0.1116*featuresmap.get("cnperc"))
						+(0.1754*featuresmap.get("cnpert"))+(0.5564*featuresmap.get("depcperc"))+(0.8829*featuresmap.get("depcpert"))+(-0.9268*featuresmap.get("coordperc"))
						+(0.1164 *featuresmap.get("coordpert"))+(-0.0247 *featuresmap.get("lexicals"))+(-4.5393*featuresmap.get("advvar"))
						+(-3.2401*featuresmap.get("adjvar"))+(-8.5029*featuresmap.get("modvar"))+(3.1289*featuresmap.get("nounvar"))
						+(2.3576*featuresmap.get("verbvar"))+(-0.0348*featuresmap.get("numconstituents"))
						+(0.0092*featuresmap.get("numsubtrees"))+(-0.0145*featuresmap.get("distsemhead"))+(-0.1741*featuresmap.get("numwh"))
						+(10.3519*featuresmap.get("rrc"))+(0.5617*featuresmap.get("adverbs"))+(-0.4474*featuresmap.get("modals"))
						+(0.0618*featuresmap.get("perpro"))+(-0.2594*featuresmap.get("whpro"))+(-5.0642*featuresmap.get("numdet"))
						+(0.7193*featuresmap.get("numvb"))+(8.8638*featuresmap.get("numvbg"))+(7.2103*featuresmap.get("numvbn"))
						+(-3.1008*featuresmap.get("numvbp"))+(0.365 *featuresmap.get("unparsable"))+(-1.3422*featuresmap.get("ttr"))
						+(0.0523*featuresmap.get("cttr"))+(2.8853*featuresmap.get("bilog"))+(-8.9353*featuresmap.get("morphcomplex"))
						+(0.4943*featuresmap.get("morphconversions"))+(10.7579*featuresmap.get("morphcontractions"))
						+(-2.0027*featuresmap.get("morphirrelevant"))+(-11.4297*featuresmap.get("morphobscure"))
						+(-11.8022*featuresmap.get("morphmayincluderoot"))+(-17.4428*featuresmap.get("morphundetermined"))
						+(-3.3491*featuresmap.get("moreanalyses"))+(-68.5213*featuresmap.get("nvaffcomp"))
						+(100.4742*featuresmap.get("comp"))+(71.792*featuresmap.get("dercomp"))+(-1.9468*featuresmap.get("intrans"))
						+(0.8504*featuresmap.get("trans"))+(-12.403*featuresmap.get("sastem"))+(0.9373*featuresmap.get("sasanda"))
						+(-12.0301*featuresmap.get("saflex"))+(13.5788*featuresmap.get("alloclip"))+(53.0865*featuresmap.get("alloflex"))
						+(-8.2808*featuresmap.get("alloconv"))+(-3.3293*featuresmap.get("transderhash"))+(-0.9953*featuresmap.get("transderadd"))
						+(-2.3676*featuresmap.get("transderremov"))+(4.5943*featuresmap.get("reversal"))+(-0.0112*featuresmap.get("numnouns"))
						+(0.0083*featuresmap.get("numpropernouns"))+(0.2231*featuresmap.get("numverb"))+(0.0602*featuresmap.get("numarticle"))
						+(0.1552*featuresmap.get("numpron"))+(-0.0231*featuresmap.get("numprep"))+(-0.1873*featuresmap.get("numconj"))
						+(1.816*featuresmap.get("numinterj"))+(0.1312*featuresmap.get("numcountablen"))+(-0.0288*featuresmap.get("numuncountablen"))
						+(-0.0977*featuresmap.get("numsingularn"))+(-0.2674*featuresmap.get("numpluraln"))+(-0.2047*featuresmap.get("numattrn"))
						+(0.7583*featuresmap.get("numExprN"))+(0.0796*featuresmap.get("transV"))+(-0.105*featuresmap.get("intransV"))
						+(0.1068*featuresmap.get("ditransV"))+(0.0813*featuresmap.get("linkingV"))+(2.1842*featuresmap.get("exprV"))
						+(-0.2238*featuresmap.get("ordAdj"))+(0.1381*featuresmap.get("attrAdj"))+(0.3285*featuresmap.get("postposAdj"))
						+(-2.4299*featuresmap.get("exprAdj"))+(-0.3027*featuresmap.get("ordAdv"))+(1.2878*featuresmap.get("predAdv"))
						+(-0.6395*featuresmap.get("postposAdv"))+(2.6745*featuresmap.get("possPro"))+(-40.4849*featuresmap.get("whPro"))
						+(-0.109*featuresmap.get("pronPro"))+(0.3294*featuresmap.get("AoA_Kup"))+(0.8836*featuresmap.get("AoA_kup_Lem"))
						+(0.054*featuresmap.get("AoA_Bird_Lem"))+(0.0777*featuresmap.get("AoA_Cort_Lem"))+(0.0869*featuresmap.get("familiarity"))
						+(0.0178*featuresmap.get("concreteness"))+(-0.0721*featuresmap.get("imagery"))+(-0.0461*featuresmap.get("colMeaningful"))
						+(-0.0487*featuresmap.get("AoA_MRC"))+6.1874;

		//System.out.println(readingLevel);
		
		if(readingLevel > 5.5)
		{
			return 5.5;
		}
		else if (readingLevel < 1.0)
		{
			return 1.0;
		}
		else
		{
			return readingLevel;
		}
	}
	
	/**
	 * Returns an appropriate age-group that maps to the reading level.
	 * @param readingLevel
	 * @return
	 */
	private static String mapReadingLeveltoAgeGroup(Double readingLevel)
	{
		String ageGroup = "";
		if(readingLevel < 1.5)
		{
			ageGroup = "7-8Years";
		}
		else if(readingLevel >=1.5 && readingLevel<2.5)
		{
			ageGroup = "8-9years";
		}
		else if(readingLevel >=2.5 && readingLevel<3.5)
		{
			ageGroup = "9-10years";
		}
		else if(readingLevel >=3.5 && readingLevel<4.5)
		{
			ageGroup = "11-14years";
		}
		else if(readingLevel>=4.5 && readingLevel <5.5)
		{
			ageGroup = "14-16years";
		}
		else
		{
			ageGroup = "16+years";
		}
		return ageGroup;
	}

	private static HashMap<String, Double> buildFeaturesMap(String featurestring) throws Exception
	{
		String featuresheader = "sent,senlen,nouns,propernouns,pronouns,conj,adj,ver,numnp,numvp,numpp,numsbar,avgnpsize,avgvpsize,avgppsize,avgparsetreeheight,numclauses,mlc,numtunits,mlt,"
			 +"cnperc,cnpert,depcperc,depcpert,coordperc,coordpert,vppert,lexicals,advvar,adjvar,modvar,nounvar,verbvar,depwords,distheadword,numconstituents,numsubtrees,"
			 +"distsemhead,numwh,rrc,interj,conjp,adverbs,modals,numsenses,commas,perpro,whpro,numfuncwords,numdet,numvb,numvbd,numvbg,numvbn,numvbp,unparsable,ttr,cttr,bilog,"
			 +"uber,numsentences,percentwordsconsidered,percentwordsnotconsidered,morphcomplex,morphmonomorphic,morphconversions,morphcontractions,morphirrelevant,morphobscure,"
			 +"morphmayincluderoot,morphundetermined,foreignwords,moreanalyses,nvaffcomp,der,comp,dercomp,intrans,trans,transintrans,unmarkedtrans,sastem,saaffix,sasanda,saflex,"
			 +"sasandflex,alloblend,alloclip,alloderiv,alloflex,alloconv,subst,opacity,transderhash,transderadd,transderremov,infix,reversal,numsentences2,percentwordsconsidered2,"
			 +"percentwordsnotconsidered2,numnouns,numpropernouns,numadj,numverb,numarticle,numpron,numadv,numprep,numconj,numinterj,numcountablen,numuncountablen,numsingularn,"
			 +"numpluraln,numgroupcount,numgroupuncount,numattrn,numpostposN,numvocN,numExprN,transV,transcompV,intransV,ditransV,linkingV,phrasalV,prepV,phrprepV,exprV,ordAdj,"
			 +"attrAdj,predAdj,postposAdj,exprAdj,ordAdv,predAdv,postposAdv,combAdv,ExpAdv,perPro,demonPro,possPro,reflPro,whPro,detPro,pronPro,expPro,coordConj,subordConj,"
			 +"AoA_Kup,AoA_kup_Lem,AoA_Bird_Lem,AoA_Bristol_Lem,AoA_Cort_Lem,familiarity,concreteness,imagery,colMeaningful,pavioMeaning,AoA_MRC";
		String[] featurenames = featuresheader.split(",");
		String[] features = featurestring.split(",");
		HashMap<String, Double> featuremap = new HashMap<String, Double>();
		for(int i=0;i<featurenames.length;i++)
		{
			featuremap.put(featurenames[i], Double.parseDouble(features[i]));
		}
		return featuremap;
	}
}
