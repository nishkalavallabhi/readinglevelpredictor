package utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import com.gargoylesoftware.htmlunit.StringWebResponse;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HtmlPage;


/**
 * 
 */

/**
 * @author svajjala
 * Parses the file given, removes HTML tags and un-necessary data 
 * Method: Use HTMLUnit.(http://htmlunit.sourceforge.net/) and the xpath "//html/body//div//text()" and get every line that has greater than 6 whitespaces. 
 * This second part is just a crude way to ensure that we are not taking any subheadings etc, which cannot be really parsed properly. This is crude..but it works!
 * Why HTMLUnit?: It does HTML parsing in a robust manner. 
 *
 */
public class parseHTMLFile {
	
	private static Scanner scanner; //Scanner object to read the file contents
	private static String fileContent; //Stores the text content of the input HTML file
	private static WebClient webClient; //WebClient object that is needed to read HTML.
	private static WebClient webClient4URL;
	private static URL url;
	private static URL urlreal;
	
	public static void main(String[] args) throws Exception
	{
	  parseHTMLFile p = new parseHTMLFile();
	  p.parseURL("http://simple.wikipedia.org/wiki/Tropical_Storm_Barry_%282007%29");
	  
	}
	
	/**
	 * This Method parses a HTML file saved on disk
	 * @param filepath - path containing the html file on disk
	 * @return text content of the HTML file.
	 * @throws Exception
	 */
	public String parseFile(String filepath) throws Exception
	{
		String parsedString = "";
		
		//First Read the file
		File file = new File(filepath);
		scanner = new Scanner(file).useDelimiter("\\Z");
		fileContent = scanner.next();
		scanner.close();
		
		//And, use HTMLUnit to parse the file
		StringWebResponse response = new StringWebResponse(fileContent, url);
		HtmlPage page = HTMLParser.parseHtml(response, webClient.getCurrentWindow());
		List<?> nodes = page.getByXPath("//html/body//div//text()");
		for(int i=0;i<nodes.size();i++)
        {
          if(nodes.get(i).toString().split(" ").length > 6)
         {
           parsedString += nodes.get(i).toString() + "\n";
         }
        }
		nodes.clear();
		
		//Now, return the parsedstring, which contains the plaintext from input html file.
		return parsedString;
	}
	
	/**
	 * This method parses a URL directly instead of a saved HTML file on disk.
	 * @param url - URL that needs to be parsed to extract HTML
	 * @return Returns the parsed content of the URL
	 * @throws Exception
	 */
	public String parseURL(String url) throws Exception
	{
		urlreal = new URL(url);
		webClient4URL = new WebClient();
		webClient4URL.setCssEnabled(false);
        webClient4URL.setJavaScriptEnabled(false);
        webClient4URL.setThrowExceptionOnFailingStatusCode(false);
        webClient4URL.setThrowExceptionOnScriptError(false);
        webClient4URL.setPrintContentOnFailingStatusCode(false);
        webClient4URL.setAppletEnabled(false);
        
		String parsedString = "";
		HtmlPage page = webClient4URL.getPage(url);
	
		String text = page.asText();
		String[] textinlines = text.split("\n");
		for(String line: textinlines)
		{
			//System.out.println(line + "   " + line.split(" ").length);
			if(line.split(" ").length >= 10)
			{
			//	System.out.println(line + "   " + line.split(" ").length);
				parsedString += line + "\n";
			}
		}
		/*
		List<?> nodes = page.getByXPath("//html/body//text()");
		for(int i=0;i<nodes.size();i++)
        {
          if(nodes.get(i).toString().split(" ").length > 4)
         {
           System.out.println(nodes.get(i).toString());
           parsedString += nodes.get(i).toString() + " ";
         }
        }
		nodes.clear();*/
		//System.out.println(parsedString);
		return parsedString;
	}
	
	/** This one is initialized each time a new file arrives. I guess this can be done only once. But, I did not think much.
	These settings are to ensure that the webClient does not throw an error just because we don't have all those JS and CSS files associated with the website.
	More such usage tips can be found on HTMLUnit forums and on the software's webpage.
	 * @throws Exception
	 */
	public void initializeHTMLParser() throws Exception
	{
		url = new URL("http://www.example.com");
		webClient = new WebClient();
		webClient.setCssEnabled(false);
        webClient.setJavaScriptEnabled(false);
        webClient.setThrowExceptionOnFailingStatusCode(false);
        webClient.setThrowExceptionOnScriptError(false);
        webClient.setPrintContentOnFailingStatusCode(false);
        webClient.setAppletEnabled(false);
	}
	

}
