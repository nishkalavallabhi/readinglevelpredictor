package features;

/**
 * @author svajjala
 * Extracts lexical, syntactic parse tree and wordnet based features from plain text.
 * Utilizes sentence splitter from MorphAdorner
 *
 */

//General Java imports
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Imports for Berkeley parser.
import edu.berkeley.nlp.PCFGLA.CoarseToFineMaxRuleParser;
import edu.berkeley.nlp.PCFGLA.Grammar;
import edu.berkeley.nlp.PCFGLA.Lexicon;
import edu.berkeley.nlp.PCFGLA.ParserData;
import edu.berkeley.nlp.PCFGLA.TreeAnnotations;
import edu.berkeley.nlp.io.PTBLineLexer;
import edu.berkeley.nlp.ling.HeadFinder;
import edu.berkeley.nlp.syntax.RichLabel;
import edu.berkeley.nlp.syntax.Tree;
import edu.berkeley.nlp.util.Numberer;

//Imports for Sentence Splitter, Wordnet and Morph Adorner.
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.morph.WordnetStemmer;
import edu.northwestern.at.utils.corpuslinguistics.lemmatizer.DefaultLemmatizer;
import edu.northwestern.at.utils.corpuslinguistics.lemmatizer.Lemmatizer;
import edu.northwestern.at.utils.corpuslinguistics.sentencesplitter.DefaultSentenceSplitter;
import edu.northwestern.at.utils.corpuslinguistics.sentencesplitter.SentenceSplitter;

//Imports for Tregex pattern matcher (which takes some imports from Stanford Parser and some from Tregex jar.
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.StringLabelFactory;
import edu.stanford.nlp.trees.CollinsHeadFinder;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.LabeledScoredTreeFactory;
import edu.stanford.nlp.trees.PennTreeReader;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.TreeReader;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.tregex.*;

public class LexSynWnFeaturesExtractor {

	/**
	 * @param args
	 */
	//Some static variables for the parser
	public static int MaxLength = 200;
	public static int nThreads=1;
	public static int kbest = 1;
	public static boolean tokenize=false;
	private static CoarseToFineMaxRuleParser parser;
	
	//Variables for Wordnet
	public static URL wnpath = null;
	public static IDictionary dict = null; 
	public static WordnetStemmer stemmer =  null;
	public static List<String> auxVerbs = null;
	
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
			
	//Was doing some trial code here...but its not necessary for the Web UI project I guess.
	}
	
	 /**
     * This is the main method in this class, which extracts a few POS, Wordnet and Parse tree based features from plaintext.
     * @param full plain text content as a String.
     * Run it through a sentence splitter first. Through Berkeley Parser next. Then Extract everything thats needed.
     */

	public String extractParseFeatures(String fulltext)
	{
	
		String resultstring = "";
		
		//Declare all necessary variables. Not writing explanations as this part seems self evident.
		int numSBAR = 0; 
  	    int avgParseTreeHeight = 0; 
    	int numNP = 0;
	    int numVP = 0;
	    int numPP = 0;
	    int numAdj = 0;
	    int numNouns = 0;
	    int numVerbs = 0;
	    int numPronouns = 0;
	    int numConjunct = 0;
	    int numProperNouns = 0;	 
	    int numPrepositions = 0;
	    int numAdverbs = 0;
	    int numLexicals = 0;
	    int numWhPhrases = 0;
	    int numConjPhrases = 0;
	    int reducedRelClauses = 0;
	    int interjections = 0;
	    int numModals = 0;
	    int numcommas = 0;
	    int perpronouns = 0; //adding num Personal Pronouns with the hypothesis that they will occur more in Simple Sentences
	    int whperpronouns = 0; //adding num Wh personal pronouns with the hypothesis that they will occur more in Normal sentences.
	    int numauxverbs = 0;
	    int numConstituents = 0; //Number of Constituents of a Parse Tree. Although I have no clue what they mean by a constituent..I am trying.

	    //Experimenting with some dependency info the parser gives: 
	    int numDependencies = 0; //Number of dependencies per word in a sentence.
	    int distHeadWord = 0; //Average index of the headword's position in a sentence
	    int numSubtrees = 0;
	    int distSemanticHeadWord = 0;
	       
	    //TTR variants cal.
	    ArrayList<String> altypes = new ArrayList<String>();
	   
	    //For SLA features.
	    double numClauses = 0.0;
	    double numComplexNominals = 0;
	    double numTunits = 0;
	    double numDependentClauses = 0.0;
	    double numCoordinateClauses = 0.0;
	    
	    double AvgNPSize = 0.0;
	    double AvgVPSize = 0.0;
	    double AvgPPSize = 0.0;
	    
	    //For WN features
	    int numSenses = 0;
	    int numHypernyms = 0;
	    int wordsforwhichsensesarecounted = 0;
	    
	    //Additional stuff from Tanguy & Tulechki - French Complexity paper
	    int numInterjections = 0; 
	    int numFunctionWords = 0; //by Wiki: Articles, Pronouns, Conjunctions, Interjections, Prep, Adverbs, Aux-Verbs.
	    int numDeterminers = 0;
	    int numTenses = 0; //Number of different tenses in the sentence. Intuition: More tenses, more difficult to understand.
	    int numVB = 0;
	    int numVBD = 0;
	    int numVBG = 0;
	    int numVBN = 0;
	    int numVBP = 0;
	    int numVBZ = 0;

	    //Extra feature: Number of sentences where the parser gave up! (Ratio of unparsable sentences to total sentences might be useful here!)
	    double unparsable = 0.0;

	    List<String> postTags = null;
	    PTBLineLexer tokenizer = new PTBLineLexer();
	    SentenceSplitter ss = new DefaultSentenceSplitter();
	    edu.stanford.nlp.trees.HeadFinder hf = new CollinsHeadFinder();
		edu.stanford.nlp.trees.HeadFinder sf = new SemanticHeadFinder();
		
		//Extract all sentences in the text.
	    List<List<String>> sentences = ss.extractSentences(fulltext);
	   
	    Double TotalSentences = (double)(sentences.size());
		int TotalWords = 0;
	    
	    for(int i=0;i<sentences.size();i++)
	    {
	    	try
	    	{
	    		Tree<String> parsedTree = TreeAnnotations.unAnnotateTree(parser.getBestConstrainedParse(sentences.get(i),postTags,null));
	    		//To convert the BerkeleyTree to StanfordTree, so that Tregex can understand whats going on!
				//Why Berkeley instead of using Stanford directly?? : Its much faster when you are doing in bulk and does not suffer from memory leaks.
		    	TreeReader tr = new PennTreeReader(new StringReader(parsedTree.toString()), new LabeledScoredTreeFactory(new StringLabelFactory()));
		    	edu.stanford.nlp.trees.Tree t= tr.readTree();
	    
		    	avgParseTreeHeight += parsedTree.getDepth(); 	
		    	//Calculate Num. NP, VP, PP and their Average Sizes. Tregex Patterns are not really necessary for this part.
		    	List<edu.stanford.nlp.trees.Tree> subtrees = t.subTreeList();
		    	numSubtrees += subtrees.size();
		    	for(edu.stanford.nlp.trees.Tree st : subtrees)
		    	{
		    		if(st.isPhrasal() && st.headTerminal(hf) != null)
		    		{
		    			if(st.label().toString().equals("NP") && st.isPrePreTerminal())
		    			{
		    				numNP++;
		    				AvgNPSize += st.numChildren();
		    			}
		    			
		    			if(st.label().toString().equals("VP"))
		    			{
		    				numVP++;
		    				AvgVPSize += st.numChildren();
		    			}
		    			
		    			if(st.label().toString().equals("PP"))
		    			{
		    				numPP++;
		    				AvgPPSize += st.numChildren();
		    			}
		    			
		    			if(st.label().toString().equals("WHNP") ||st.label().toString().equals("WHPP") || st.label().toString().equals("WHAVP") || st.label().toString().equals("WHADJP"))
		    			{
		    				numWhPhrases++;
		    			}
		    			
		    			if(st.label().toString().equals("RRC"))
		    			{
		    				reducedRelClauses++;
		    			}
		    			
		    			if(st.label().toString().equals("INTJ") || st.label().toString().equals("UH"))
		    			{
		    				interjections++;
		    			}
		    			
		    			if(st.label().toString().equals("CONJP"))
		    			{
		    				numConjPhrases++;
		    			}
		    		}
		    	}
		    	
		    	//The Tregex Mania begins! These tregex patterns are from Xiaofei Lu's paper.
		    	numSBAR = numSBAR + countOccurences(t, "SBAR");
		    	numClauses = numClauses + countOccurences(t, "S|SBAR|SINV < (VP <# VBD|VBP|VBZ|MD)");
		    	numTunits = numTunits + countOccurences(t, "S|SBARQ|SINV|SQ !> (S|SINV|SBAR|SQ) |> ROOT | [$-- S|SBARQ|SINV|SQ !>> SBAR|VP]");
		    	numDependentClauses = numDependentClauses + countOccurences(t, "SBAR < (S|SINV|SQ < (VP [<# MD|VBP|VBZ|VBD | < (VP <# (MD|VBP|VBZ|VBD))]))");
		    	numCoordinateClauses = numCoordinateClauses + countOccurences(t, "ADJP|ADVP|NP|VP<CC");
		    	
		    	//Three CN patterns.
		    	numComplexNominals = numComplexNominals + countOccurences(t, "NP !> NP [<< JJ|POS|PP|S|VBG | << (NP $++ NP !$+ CC)]");
		    	numComplexNominals = numComplexNominals + countOccurences(t, "SBAR [<# WHNP | <# (IN < That|that|For|for) | <, S] & [$+ VP | > VP]");
		    	numComplexNominals = numComplexNominals + countOccurences(t, "S < (VP <# VBG|TO) $+ VP");
		    	   	
		    	
		        //Count various POS occurences. Calcuate the Num of various POS Tags etc.
				List<Tree<String>> preT = parsedTree.getPreTerminals();
				for (Tree t1 : preT) {
					String tag = t1.getLabel().toString();
					String word = t1.getTerminals().get(0).toString();
					
					if(!altypes.contains(word))
					{
						altypes.add(word);
					}
					
					String generaltag = null;
									
					if(tag.equals("PRP") || tag.equals("PRP$") || tag.equals("WP") || tag.equals("WP$"))
					{
						numPronouns++;
						if(tag.equals("PRP"))
						{
							perpronouns++;
						}
						if(tag.equals("WP"))
						{
							whperpronouns++;
						}
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("VB") || tag.equals("VBD") || tag.equals("VBG") || tag.equals("VBN") || tag.equals("VBP") || tag.equals("VBZ"))
					{
						numVerbs++;
						
						generaltag = "VERB";
						TotalWords++;
						if(tag.equals("VB"))
						{
							numVB++;
						}
						else if(tag.equals("VBD"))
						{
							numVBD++;
						}
						else if(tag.equals("VBG"))
						{
							numVBG++;
						}
						else if(tag.equals("VBN"))
						{
							numVBN++;
						}
						if(tag.equals("VBP"))
						{
							numVBP++;
						}
						if(tag.equals("VBZ"))
						{
							numVBZ++;
						}
					}
					if(tag.equals("JJ") || tag.equals("JJR") || tag.equals("JJS"))
					{
						numAdj++;
						generaltag = "ADJECTIVE";
						TotalWords++;
					}
					if(tag.equals("RB") || tag.equals("RBR") || tag.equals("RBS") || tag.equals("RP"))
					{
						numAdverbs++;
						generaltag = "ADVERB";
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("IN"))
					{
						numPrepositions++;
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("UH"))
					{
						numInterjections++;
						interjections++;
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("CC"))
					{
						numConjunct++;
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("NN") || tag.equals("NNS"))
					{
						
						numNouns++;
						generaltag = "NOUN";
						TotalWords++;
					}
					if(tag.equals("NNP") || tag.equals("NNPS"))
					{
						numProperNouns++;
						generaltag = "PROPERNOUN";
						TotalWords++;
					}
					if(tag.equals("MD"))
					{
						numModals++;
						numauxverbs++;
						numFunctionWords++;
						TotalWords++;
					}
					if(tag.equals("DT"))
					{
						numFunctionWords++;
						numDeterminers++;
						TotalWords++;
					}
										
					
					if(generaltag != null)
					{
						try{
							// get Number of senses for this word, tag pair:
							String lemma = stemmer.findStems(word, POS.valueOf(generaltag)).get(0);
							if(auxVerbs.contains(lemma))
							{
								numauxverbs++;
								numFunctionWords++;
							}
							if(!auxVerbs.contains(lemma) && !generaltag.equals("PROPERNOUN")) //Calculate no. of senses only if the word is not an aux. verb.
							{
								IIndexWord idxWord = dict.getIndexWord(lemma,POS.valueOf(generaltag));
								numSenses += idxWord.getWordIDs().size();
								wordsforwhichsensesarecounted++;		
							}
						}
						catch(Exception wnException)
						{
						//	System.out.println("Error while handling Wordnet: " + wnException.toString());		
						//	continue;
						}
					//Note: Wordnet features are extracted using edu.mit.jwi...jar file. Their user manual is at: 
					//http://projects.csail.mit.edu/jwi/download.php?f=edu.mit.jwi_2.3.0_manual.pdf
					}
					
				}
				
				numLexicals += numAdj+numNouns+numVerbs+numAdverbs+numProperNouns; //Lex.Den = NumLexicals/TotalWords
				distHeadWord += sentences.get(i).indexOf(t.headTerminal(hf).toString());
				distSemanticHeadWord += sentences.get(i).indexOf(t.headTerminal(sf).toString());
				numConstituents += t.constituents().size();				
	    	
	    	}
	    	catch(Exception e2)
	    	{
	    		unparsable++;
	    		System.out.println("Error while parsing the sentence: " + sentences.get(i));
	    		unparsable++;
	    		//Some sentences are unparsable by Berkeley Parser. Counting them just to have an estimate if necessary.
	    	}
	    	
	    	
	    }
	   
	    double numtypes = altypes.size(); //numtokens=totalwords.

	    //Now, extract the features only if TotalWords != 0; If it is, there is no point in doing all this. 
	    if(TotalWords != 0)
	    {
	    	resultstring = TotalSentences+ ","+restrict2TwoDecimals(TotalWords/TotalSentences)
		  +","+restrict2TwoDecimals((double)(numNouns+numProperNouns)/TotalWords)+","+restrict2TwoDecimals((double)numProperNouns/TotalWords)
		  +","+restrict2TwoDecimals((double)numPronouns/TotalWords)+","+restrict2TwoDecimals((double)numConjunct/TotalWords)
		  +","+restrict2TwoDecimals((double)numAdj/TotalWords)+","+restrict2TwoDecimals((double)numVerbs/TotalWords)
		  +","+restrict2TwoDecimals((double)numNP/TotalSentences)+","+restrict2TwoDecimals((double)numVP/TotalSentences)
		  +","+restrict2TwoDecimals((double)numPP/TotalSentences)+","+restrict2TwoDecimals((double)numSBAR/TotalSentences)
		  +","+handleDivByZero(AvgNPSize,numNP)+","+handleDivByZero(AvgVPSize,numVP)
		  +","+handleDivByZero(AvgPPSize,numPP)+","+restrict2TwoDecimals(avgParseTreeHeight/TotalSentences)
		  +","+restrict2TwoDecimals(numClauses/TotalSentences)+","+handleDivByZero((double)TotalWords, numClauses)
		  +","+restrict2TwoDecimals(numTunits/TotalSentences)+","+handleDivByZero((double)TotalWords,numTunits)
		  +","+handleDivByZero(numComplexNominals,numClauses)+","+handleDivByZero(numComplexNominals,numTunits)
		  +","+handleDivByZero(numVP,numTunits)
		  +","+handleDivByZero(numDependentClauses,numClauses)+","+handleDivByZero(numDependentClauses,numTunits)
		  +","+handleDivByZero(numCoordinateClauses,numClauses)+","+handleDivByZero(numCoordinateClauses,numTunits)
		  +","+restrict2TwoDecimals((double)numLexicals/TotalWords)+","+restrict2TwoDecimals((double)numAdverbs/numLexicals)
		  +","+restrict2TwoDecimals((double)numAdj/numLexicals)+","+restrict2TwoDecimals((double)(numAdj+numAdverbs)/numLexicals)
		  +","+restrict2TwoDecimals((double)(numNouns+numProperNouns)/numLexicals)+","+restrict2TwoDecimals(Math.abs((double)(numVerbs-numauxverbs))/numLexicals)
		  +","+restrict2TwoDecimals((double)numDependencies/TotalWords)+","+restrict2TwoDecimals(distHeadWord/TotalSentences)
		  +","+restrict2TwoDecimals((double)numConstituents/TotalSentences)+","+restrict2TwoDecimals((double)numSubtrees/TotalSentences)
		  +","+restrict2TwoDecimals((double)distSemanticHeadWord/TotalSentences)+","+restrict2TwoDecimals((double)numWhPhrases/TotalSentences)
		  +","+restrict2TwoDecimals((double)reducedRelClauses/TotalSentences)+","+restrict2TwoDecimals((double)interjections/TotalSentences)
		  +","+restrict2TwoDecimals((double)numConjPhrases/TotalSentences) +","+restrict2TwoDecimals((double)numAdverbs/TotalSentences)
		  +","+restrict2TwoDecimals((double)numModals/TotalSentences)+","+handleDivByZero((double)numSenses,wordsforwhichsensesarecounted)
		  +","+restrict2TwoDecimals((double)numcommas/TotalSentences)+","+restrict2TwoDecimals((double)perpronouns/TotalSentences)
		  +","+restrict2TwoDecimals((double)whperpronouns/TotalSentences)+","+restrict2TwoDecimals((double)numFunctionWords/TotalWords)
		  +","+restrict2TwoDecimals((double)numDeterminers/TotalWords)+","+restrict2TwoDecimals((double)numVB/TotalWords)
		  +","+restrict2TwoDecimals((double)numVBD/TotalWords)+","+restrict2TwoDecimals((double)numVBG/TotalWords)
		  +","+restrict2TwoDecimals((double)numVBN/TotalWords)+","+restrict2TwoDecimals((double)numVBP/TotalWords)
		  +","+restrict2TwoDecimals(unparsable/TotalSentences)+","+restrict2TwoDecimals(numtypes/TotalWords)
		  +","+restrict2TwoDecimals(numtypes/(Math.sqrt(2*TotalWords)))+","+restrict2TwoDecimals(Math.log(numtypes)/Math.log(TotalWords))
		  +","+restrict2TwoDecimals(Math.log(numtypes)*Math.log(numtypes)/(Math.log(TotalWords/numtypes)))
		  ;
	    }
	    else
	    {
	    	resultstring = "NotProcessed";
	    }
	    
	    
		return resultstring;
	}
	
	/**
	 * Constructor. Initializes required classes and parameters too.
	 * @throws IOException
	 */
	public LexSynWnFeaturesExtractor() throws IOException
	{
		initialize();
	}
	
	/**
	 * Initialization parameters for parser, wordnet.
	 * @throws IOException
	 */
	private static void initialize() throws IOException
	{
		//Initialization of Berkeley Parser. 
		String inFileName = "db/eng_sm6.gr"; //Indicates the GrammarFilePath for parser. Should change.
		System.out.println("Loading the parser");
		ParserData pData = ParserData.Load(inFileName);
		System.out.println("Loaded the parser!");
		if(pData==null)
		{
			System.out.println("Failed to load grammar from file:" + inFileName);
			System.exit(1);
		}
		Grammar grammar = pData.getGrammar();
		Lexicon lexicon = pData.getLexicon();
		Numberer.setNumberers(pData.getNumbs());
		System.out.println("Initializing the parser now!");
		parser = new CoarseToFineMaxRuleParser(grammar, lexicon, 1.0,-1,true,false,false, true, false, true, true);
		/* End of Berkeley Parser setup. For more details on this, look at Berkeley Parser source code. There is no real documentation for it as of now.
		I just copied the way source code responds to command line usage of the parser.*/
		
		//Wordnet related initializations, following the User manual.
		wnpath = prepareWNUrl();
		// construct the dictionary object and open it
		dict = new Dictionary(wnpath);
		dict.open();
		stemmer = new WordnetStemmer(dict);
		
		//Populating the list of Auxiliary verbs, for Wordnet related features extraction.
		String[] auxverbslist = {"be","can","could","do","have","may","might","must","shall","should","will","would"};
		auxVerbs = Arrays.asList(auxverbslist);
		
	}
	
	/** 
	 * preparing the WN file path in a way the API can understand.
	 * @return WNUrl value string.
	 */
	private static URL prepareWNUrl()
	{
		String wnpath = "db/dict";
		URL url = null;
		try{ url = new URL("file", null, wnpath); } 
		catch(MalformedURLException e){ e.printStackTrace(); }
		return url;
	}

	/**
	 * Restricting feature values to two decimals because WEKA cannot handle over specification sometimes! 
	 * @param Double value that should be restricted to two decimals.
	 * @return Double value with only 2 decimals
	 */
	private static double restrict2TwoDecimals(double D)
	{
		int temp = (int)(D*100);
		return (double)(temp/100.0);
	}
	
	/**
	 *  In the exceptional cases where something happens and a division by zero takes place, just make it zero. 
	 * This is not the best solution..but I couldn't think of a better quick and dirty solution!
	 * @param two double values ..first one is the dividend, second is the divisor. 
	 * @return quotient
	 */
	private static double handleDivByZero(double i1, double i2)
	{
		if(i2 == 0.0)
		{
			return 0.0;
		}
		else
		{
			return restrict2TwoDecimals(i1/i2);
		}
	}

	/**
	 *  Counts the occurences of a given pattern in a tree! Use a tregexpattern matcher.
	 * @param tregex Pattern
	 */
    private static int countOccurences(edu.stanford.nlp.trees.Tree tree, String pattern) throws ParseException
    {
    	TregexPatternCompiler tpc = new TregexPatternCompiler();
    	TregexPattern p0 = tpc.compile(pattern);
    	TregexMatcher m0 = p0.matcher(tree);
    	int count = 0;
    	while(m0.find())
		{
		     count++;
		}
    	return count;
    }
}
