package features;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * 
 */

/**
 * @author svajjala
 * Get word AoA (Age of Acquistion) Norms based features using several AoA estimations on the MRC site.
 *
 */
public class AoANormFeaturesExtractor {

	/**
	 * @param args
	 */
	private static MaxentTagger met; 
	private static Hashtable aoaDb = new Hashtable<String, String>(100000);
	private static Hashtable mrcdb = new Hashtable<String, String>(200000);
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		initialize();
		System.out.println(aoaDb.get("crazy").toString());
		System.out.println(mrcdb.get("crazy").toString());
		
	}
	
	/**
	 * This method extracts Age of Acq. features and MRC Psycholinguistic Database based features from Plaintext.
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public static String getAoAMRCFeatures(String text) throws Exception
	{
		if(aoaDb.isEmpty() || mrcdb.isEmpty())
		{
			System.out.println("You forgot to initialize the databases. Run the initialize method before calling this method!");
		}
		
		String[] temp = null;

		//Declare all the necessary number variables:
		//From Aoa: AoA_Kup,AoA_kup_Lem,AoA_Bird_Lem,AoA_Bristol_Lem,AoA_Cort_Lem
		double AoA_Kup = 0.0;
		double AoA_Kup_Lem = 0.0;
		double AoA_Bird_Lem = 0.0;
		double AoA_Bristol_Lem = 0.0;
		double AoA_Cort_Lem = 0.0;
	//	double AoA_Schock = 0.0;
		
		//From MRC: familiarity,concreteness,imagery,colorado meaningfulness,pavio meaningfulness,AoA
		double MrcFam = 0.0;
		double MrcConc = 0.0;
		double MrcImag = 0.0;
		double MrcColMean = 0.0;
		double MrcPavioMean = 0.0;
		double MrcAoA = 0.0;
		
		//A variable to count the number of words that got all these characteristics. Useful to estimate the average.
		int numAoAwords = 0;
		int numMrcWords = 0;
		
		String result="none";
		
		try{
			temp= met.tagString(text).split(" "); 
		}
		catch(Exception e)
		{
			System.out.println("Exception while tagging." + e.toString() + "   ");
			System.exit(1);	
		}
		
		//Next, take words_tags format one by one, get these features. Estimate the averages towards the end.
		for(String wordtagpair : temp)
		{
			String[] temppair = wordtagpair.split("_");
			String word = temppair[0];
			
			//AoA_Kup,AoA_kup_Lem,AoA_Bird_Lem,AoA_Bristol_Lem,AoA_Cort_Lem
			if(aoaDb.containsKey(word.toLowerCase()))
			{
				numAoAwords++;
				String[] variousaoavalues = aoaDb.get(word.toLowerCase()).toString().replaceAll("none", "0").split(",");
				
				AoA_Kup += Double.parseDouble(variousaoavalues[0]);
				AoA_Kup_Lem += Double.parseDouble(variousaoavalues[1]);
				AoA_Bird_Lem += Double.parseDouble(variousaoavalues[2]);
				AoA_Bristol_Lem += Double.parseDouble(variousaoavalues[3]);
				AoA_Cort_Lem += Double.parseDouble(variousaoavalues[4]);
			}
			
			
			//familiarity,concreteness,imagery,colorado meaningfulness,pavio meaningfulness,AoA
			if(mrcdb.containsKey(word.toLowerCase()))
			{
				numMrcWords++;
				String[] variousmrcvalues = mrcdb.get(word.toLowerCase()).toString().replaceAll("none", "0").split(",");
				
				MrcFam += Double.parseDouble(variousmrcvalues[0])/7;
				MrcConc += Double.parseDouble(variousmrcvalues[1])/7;
				MrcImag += Double.parseDouble(variousmrcvalues[2])/7;
				MrcColMean += Double.parseDouble(variousmrcvalues[3])/7;
				MrcPavioMean += Double.parseDouble(variousmrcvalues[4])/7;
				MrcAoA += Double.parseDouble(variousmrcvalues[5])/7;
				
			}
			
			result = restrict2TwoDecimals(AoA_Kup/numAoAwords) + "," + restrict2TwoDecimals(AoA_Kup_Lem/numAoAwords)
							+"," + restrict2TwoDecimals(AoA_Bird_Lem/numAoAwords)+"," + restrict2TwoDecimals(AoA_Bristol_Lem/numAoAwords)
							+"," + restrict2TwoDecimals(AoA_Cort_Lem/numAoAwords)+","+restrict2TwoDecimals(MrcFam/numMrcWords)
							+","+restrict2TwoDecimals(MrcConc/numMrcWords)+","+restrict2TwoDecimals(MrcImag/numMrcWords)
							+","+restrict2TwoDecimals(MrcColMean/numMrcWords)+","+restrict2TwoDecimals(MrcPavioMean/numMrcWords)
							+","+restrict2TwoDecimals(MrcAoA/numMrcWords);
			
		}
		return result;
	}
	
	/**
	 * Initializes the class by building MRCDb and AoANormsDb from the disk. Call this method before extracting features from the above method!
	 * Builds a hash table with the word as the key and all the requisite numbers as its comma-seperated value string..
	 */
	public static void initialize() throws Exception
	{
		//tagger initialization
		met = new MaxentTagger("db/wsj-0-18-left3words.tagger"); 
		//(Actually, why do I need a tagger here??)
		
		//Everything from AoA data. This CSV file can be understood if you browse through the Kuperman et.al. paper.
		String aoa50Kpath = "db/AoA_51715_words_copy.csv";
		BufferedReader br = new BufferedReader(new FileReader(aoa50Kpath));
		br.readLine(); //To read the header.
		//End string: "word": AoA_Kup,AoA_kup_Lem,AoA_Bird_Lem,AoA_Bristol_Lem,AoA_Cort_Lem,AoA_Schock
		String dummy = "";
		while((dummy=br.readLine())!=null)
		{
			String[] pair = dummy.split(",");
			String key = pair[0];
			String value = pair[8]+","+pair[10]+","+pair[12]+","+pair[13]+","+pair[14]; //+","+pair[15];
			aoaDb.put(key,value);
		}
		br.close();
		
		//From MRC Psycholing DB. Google and read about this too :-)
		String mrcfilepath = "db/mrcdictfull.csv";
		br =  new BufferedReader(new FileReader(mrcfilepath));
		
		//End value string: familiarity,concreteness,imagery,colorado meaningfulness,pavio meaningfulness,AoA
		while((dummy=br.readLine())!=null)
		{
			String[] pair = dummy.split(",");
			String key = pair[22].toLowerCase();
			String value = pair[8]+","+pair[9]+","+pair[10]+","+pair[11]+","+pair[12]+","+pair[13];
			if(!mrcdb.containsKey(key))
			{
				mrcdb.put(key, value);
			}
		}
		br.close();
		
		System.out.println(aoaDb.size() + "...AoA DB size");
		System.out.println(mrcdb.size()+ "....MRD Db size");
		
	}
	
	private static double restrict2TwoDecimals(double D)
	{
		int temp = (int)(D*100);
		return (double)(temp/100.0);
	}

}
