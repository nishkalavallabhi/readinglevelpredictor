package features;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.morph.WordnetStemmer;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations.TemporalModifierGRAnnotation;

import initcodes.celexInterface;
/**
 * 
 */

/**
 * @author svajjala
 */
public class CelexMorphFeaturesExtractor {

	private static IDictionary dict; 
	private static WordnetStemmer stemmer;
	private static MaxentTagger met;
	private static celexInterface cim;
	private static Hashtable celexmorphdb = new Hashtable<String, String>(100000);
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
	  //Removed this part as it just takes a list of files and does feature estimation recursively, which is not needed for our project.	
	}
	
	/**
	 * Takes a text string. Returns a Comma seperated string of Celex Morph-Feature values for that Text Document.
	 * Morph Feature string order: in the order in which feature values are defined below. For some commentary on Celex, see the comments in CelexSynFeaturesExtractor.java
	 * @param filecontent - content of the file :p
	 * @param filepath - any filepath will do because its just to keep track of the file while training the machine learner. Now, thats not needed.
	 * @return
	 */
	public static String getMorphFeatures(String filecontent,String filepath)
	{
		//First Tag the content using Stanford Tagger.
		//Even a sentence splitter does not seem to be necessary for this purpose since the tagger seems to understand sentence seperations.
	    
		String[] temp = null;
		try{
			temp= met.tagString(filecontent).split(" "); 
		}
		catch(Exception e)
		{
			System.out.println("Exception while tagging." + e.toString() + "   " + filepath);
			System.exit(1);	
		}

		String tempStringofInfo = ""; //Temporary string that is used to extract Values from the morpheme Dictionary.
		
		//Declare all the variables to calculate feature values (34 in All, for now)
		
		//Basics 
		double numSentences = 0.0;
		double numWordsConsidered = 0.0;
		double numWordsNotConsidered = 0.0;
	
		//MorphStatus
		int numMorphComplex = 0;//MorphStatus="C"
		int numMorphMonoMorphemic = 0; //MorphStatus="M"
		int numMorphConversions = 0; //MorphStatus="Z"
		int numMorphContractedForm = 0; //MorphStatus="F"
		int numMorphIrrelevant = 0; //MorphStatus="I"
		int numMorphObscure = 0; //MorphStatus="O"
		int numMorphMayIncludeRoot = 0; //MorphStatus="R"
		int numMorphUndetermined = 0; //MorphStatus="U"
		
		//LangLemma - to tell words of Foreign origin?
		int numForeignOrigins = 0; //F,D,G,I,L,S: French, German, Greek, Italian, Latin and Spanish origins. A-B: American British Englishs.
		//Hence, ++ for all ForeignOrigin words and Leave A-B without change.
		
		//MorphCnt - No of Morphological Analyses for a word.
		int numWordsWithMoreAnalyses = 0;
		
		//Noun Verb Affix Compound
		int nvaffcomp = 0; ///Noun-Verb-Affix-Compound ++ if its "Y"
		int der = 0; //Derivation. ++ if its "Y"
		int comp = 0; //Compound. ++ if its "Y"
		int dercomp = 0; //Derivational Compound. ++ if its "Y"
			
		//Immediete Segmentation Related parameters
		//ImmSubcat related values:
		int numInTransitive = 0; //++ if ImmSubcat contains: "1"
		int numTransitive = 0; //++ if ImmSubcat contains: "2"
		int numTransIntrans = 0; ////++ if ImmSubcat contains: "3"
		int numUnmarkedTransitivity = 0; ////++ if ImmSubcat contains: "0"
		
		//ImmSALemma values based variables:
		int immSaStem = 0; //++ if immSA="S"
		int immSaAffix = 0; //++ if immSA=A"
		int immSaSandA = 0; //++if immSA=SA
		int immSaFlectional = 0; //++ if immSA=F
		int immSaSandF=0; //++if immSA=SF
		//Stem Allomorphy related parameters
		int immAlloBlend = 0; //++ if immAllo =B
		int immAlloClipping = 0; //++if immAllo=C
		int immAlloDeriv = 0; //++if immAllo=D
		int immAlloFlec = 0; //++ if immAllo=F
		int immAlloConv = 0; //++if immAllo=Z
		
		int immSubst = 0; //Affix Substitution. ++ if its "Y"
		int immOpacity = 0; //Opacity of the word. ++ if its "Y"
		
		//TransDer Related parameters
		int TransDerHash = 0; //++ if TransDer has only #. Derivational Transformation.
		int TransDerRemoved = 0; //++ if TransDer contains "-". No of additions = No of "-" in the string
		int TransDerAddition = 0; //++ if TransDer contains "+". No of additions = No of "+" in the string
		
		int immInfix = 0; //Infixation. ++ if its "Y"
		int immReversion = 0; //Reversion. ++ if its "Y"
		
		//Next, take words_tags format one by one, Lemmatize and get relevant info from CELEX
		for(String wordtagpair : temp)
		{
			String[] temppair = wordtagpair.split("_");
			
			
			String generaltag = "NOTAG"; //This is to give to the Lemmatizer.
			
			String word = temppair[0].toLowerCase();
			String tag = temppair[1];
			String lemma;
			String morpheme;
			
		
			
			if(tag.equals("VB") || tag.equals("VBD") || tag.equals("VBG") || tag.equals("VBN") || tag.equals("VBP") || tag.equals("VBZ"))
			{
       			generaltag = "VERB";
			}
			
			else if(tag.equals("JJ") || tag.equals("JJR") || tag.equals("JJS"))
			{
				generaltag = "ADJECTIVE";
			}
			else if(tag.equals("RB") || tag.equals("RBR") || tag.equals("RBS"))
			{
				generaltag = "ADVERB";
			}
		/*	else if(tag.equals("PRP") || tag.equals("PRP$") || tag.equals("WP") || tag.equals("WP$"))
			{
				generaltag = "PRONOUN";
			} */
			
			else if(tag.equals("NN") || tag.equals("NNS"))
			{
				generaltag = "NOUN";
			}
			
			else if(tag.equals("."))
			{
				numSentences++;
			}
			
			
		 if(!generaltag.equals("NOTAG"))
		 {
				
			if(stemmer.findStems(word, POS.valueOf(generaltag)).size() > 0)
			{
			 lemma = stemmer.findStems(word, POS.valueOf(generaltag)).get(0);	
			}
			else
			{
				lemma = word;
			}
			//If the Lemma exists in MorphemeDb
			if(celexmorphdb.containsKey(lemma.toString()))
			{				
				morpheme = lemma;
			}
			
			//IF the word directly exists in the morphemedb
			else if(celexmorphdb.containsKey(word))
			{
				morpheme = word;
				
			}
			
			//Just ignore this process for this word!
			else
			{
				morpheme = " ";
				numWordsNotConsidered++;
			}
			
			
			if(!morpheme.equals(" "))
			{
				tempStringofInfo = celexmorphdb.get(morpheme).toString();	
				String[] features = tempStringofInfo.split("\\|");
				//Morphological Status based feature values extraction
				if(features[1].equals("C"))
				{
					numMorphComplex++;
				}
				else if(features[1].equals("M"))
				{
					numMorphMonoMorphemic++;
				}
				else if(features[1].equals("Z"))
				{
					numMorphConversions++;
				}
				else if(features[1].equals("F"))
				{
					numMorphContractedForm++;
				}
				else if(features[1].equals("I"))
				{
					numMorphIrrelevant++;
				}
				else if(features[1].equals("O"))
				{
					numMorphObscure++;
				}
				else if(features[1].equals("R"))
				{
					numMorphMayIncludeRoot++;
				}
				else if(features[1].equals("U"))
				{
					numMorphUndetermined++;
				}
				else
				{
					numMorphUndetermined++; //Temporarily, Assigning "U" to an empty value in the DB: Should check if this possibility really occurs in it.
				}
				
				//Language Origin Feature
				if(!(features[2].equals("A") || features[1].equals("B")))
				{
					numForeignOrigins++;
				}
				
				//Number of words with more than one Morphological Analyses.
				if(Integer.parseInt(features[3]) > 1)
				{
					numWordsWithMoreAnalyses++;
				}
				
				//NVAffComp related features
				if(features[4].equals("Y"))
				{
					nvaffcomp++;
				}
				if(features[5].equals("Y"))
				{
					der++;
				}
				if(features[6].equals("Y"))
				{
					comp++;
				}
				if(features[7].equals("Y"))
				{
					dercomp++;
				}
				
				//features[8] : stands for Default Analysis : Ignoring this for now. Because I did not decide on how to use it.
				//features[9] : stands for Immediete Segmentation : Not useful for this business.
				if(features.length > 9)
				{
				//Extracting transitivity Information here.
					if(features[10].contains("1"))
					{
						numInTransitive++;
					}
					else if(features[10].contains("2"))
					{
						numTransitive++;
					}
					else if(features[10].contains("1"))
					{
						numTransIntrans++;
					}
					else if(features[10].contains("1"))
					{
						numUnmarkedTransitivity++;
					}
					else
					{
					//Dont do anything. No Transitivity Info. Not a verb.
					}
				
				//Immediete Segmentation related Info, from features[11]
					if(features[11].equals("S"))
					{
						immSaStem++;
					}
					else if(features[11].equals("A"))
					{
						immSaAffix++;
					}
					else if(features[11].equals("SA") || features[11].equals("AS"))
					{
						immSaSandA++;
					}
					else if(features[11].equals("F"))
					{
						immSaFlectional++;
					}
					else if(features[11].equals("SF"))
					{
						immSaSandF++;
					}
					else
					{
					//Dont do anything.
					}
				
					//Imm Stem Allomorphy related info from features[12]
					if(features[12].equals("B"))
					{
						immAlloBlend++;
					}
					else if(features[12].equals("C"))
					{
						immAlloClipping++;
					}
					else if(features[12].equals("D"))
					{
						immAlloDeriv++;
					}
					else if(features[12].equals("F"))
					{
						immAlloFlec++;
					}
					else if(features[12].equals("Z"))
					{
						immAlloConv++;
					}
					else
					{
					//Nothing...or did not decide yet.
					}
				
					//features[13]:immSubst
					if(features[13].equals("Y"))
					{
						immSubst++;
					}
					//features[14]:immOpac
					if(features[14].equals("Y"))
					{
						immOpacity++;
					}
				
					//features[15]:TransDer
					if(features[15].equals("#"))
					{
						TransDerHash++;
					}
					else
					{
						if(features[15].contains("-"))
						{
							TransDerRemoved++;
						}
						if(features[15].contains("+"))
						{
						TransDerAddition++;
						}
					}	
				
					//features[16]:ImmInfix, features[17]:ImmReversal
					if(features[16].equals("Y"))
					{
						immInfix++;
					}
					if(features[17].equals("Y"))
					{
						immReversion++;
					}
				}
				
				numWordsConsidered++;				
				
			}
		  }
		 
		 else
		 {
			 numWordsNotConsidered++;
		 }
		}
		
	
		//Now Collect all info gathered sofar, in to a string and return it to the main program!
		double totalwords = numWordsConsidered + numWordsNotConsidered;
		
		if(numSentences == 0 && totalwords > 0)
		{
			numSentences = 1;
		}
		
		String result = numSentences + "," + restrict2TwoDecimals(numWordsConsidered/totalwords) + "," + restrict2TwoDecimals(numWordsNotConsidered/totalwords)
						+","+restrict2TwoDecimals(numMorphComplex/numWordsConsidered)+","+restrict2TwoDecimals(numMorphMonoMorphemic/numWordsConsidered)
						+","+restrict2TwoDecimals(numMorphConversions/numWordsConsidered)+","+restrict2TwoDecimals(numMorphContractedForm/numWordsConsidered)
						+","+restrict2TwoDecimals(numMorphIrrelevant/numWordsConsidered)+","+restrict2TwoDecimals(numMorphObscure/numWordsConsidered)
						+","+restrict2TwoDecimals(numMorphMayIncludeRoot/numWordsConsidered)+","+restrict2TwoDecimals(numMorphUndetermined/numWordsConsidered)
						+","+restrict2TwoDecimals(numForeignOrigins/numWordsConsidered)+","+restrict2TwoDecimals(numWordsWithMoreAnalyses/numWordsConsidered)
						+","+restrict2TwoDecimals(nvaffcomp/numWordsConsidered)+","+restrict2TwoDecimals(der/numWordsConsidered)
						+","+restrict2TwoDecimals(comp/numWordsConsidered)+","+restrict2TwoDecimals(dercomp/numWordsConsidered)
						+","+restrict2TwoDecimals(numInTransitive/numWordsConsidered)+","+restrict2TwoDecimals(numTransitive/numWordsConsidered)
						+","+restrict2TwoDecimals(numTransIntrans/numWordsConsidered)+","+restrict2TwoDecimals(numUnmarkedTransitivity/numWordsConsidered)
						+","+restrict2TwoDecimals(immSaStem/numWordsConsidered)+","+restrict2TwoDecimals(immSaAffix/numWordsConsidered)
						+","+restrict2TwoDecimals(immSaSandA/numWordsConsidered)+","+restrict2TwoDecimals(immSaFlectional/numWordsConsidered)
						+","+restrict2TwoDecimals(immSaSandF/numWordsConsidered)+","+restrict2TwoDecimals(immAlloBlend/numWordsConsidered)
						+","+restrict2TwoDecimals(immAlloClipping/numWordsConsidered)+","+restrict2TwoDecimals(immAlloDeriv/numWordsConsidered)
						+","+restrict2TwoDecimals(immAlloFlec/numWordsConsidered)+","+restrict2TwoDecimals(immAlloConv/numWordsConsidered)
						+","+restrict2TwoDecimals(immSubst/numWordsConsidered)+","+restrict2TwoDecimals(immOpacity/numWordsConsidered)
						+","+restrict2TwoDecimals(TransDerHash/numWordsConsidered)+","+restrict2TwoDecimals(TransDerAddition/numWordsConsidered)
						+","+restrict2TwoDecimals(TransDerRemoved/numWordsConsidered)+","+restrict2TwoDecimals(immInfix/numWordsConsidered)
						+","+restrict2TwoDecimals(immReversion/numWordsConsidered); 
		
		return result;
	}

	private static String getMorphFeatures(String filecontent)
	{
		return getMorphFeatures(filecontent, "filename");
	}
	
	public static void initialize() throws Exception
	{
		//All Hardcoded Filepaths also exist in this method.
		//Tagger initialization (To give to Lemmatizer)
		met = new MaxentTagger("db/wsj-0-18-left3words.tagger"); //Path to Tagger Model.
		
		//Wordnet Initialization, for Lemmatizer purposes (To get Lemma and search in celexdb)
		String wnpath = "db/dict"; //Path for WN Dictionary
		URL url = null;
		try{ url = new URL("file", null, wnpath); } 
		catch(MalformedURLException e){ e.printStackTrace(); }
		if(url == null) return;
		dict = new Dictionary(url);
		dict.open();
		stemmer = new WordnetStemmer(dict);
		System.out.println("Setup wordnet only to use the lemmatizer part of it for now! ");
		
		
		//Load Celex Morphology Data
		cim = new celexInterface();
		String celexMorphFilePath =  "db/eml.cd"; //Path for Celex Morphology-Lemma DB.
		celexmorphdb = cim.createHashTable(celexMorphFilePath);
		System.out.println("Loaded Celex Morph DB of size :"+celexmorphdb.size() + " word lemmas");
		
	}
	
	private static double restrict2TwoDecimals(double D)
	{
		int temp = (int)(D*1000);
		return (double)(temp/1000.0);
	}
}
