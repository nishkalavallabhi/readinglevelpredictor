package features;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.List;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.morph.WordnetStemmer;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import initcodes.celexInterface;

/**
 * 
 */

/**
 * @author svajjala
 * Extracts Syntactic Features (based on POS tags) from CELEX
 */
public class CelexSynFeaturesExtractor {

	private static IDictionary dict; 
	private static WordnetStemmer stemmer;
	private static MaxentTagger met;
	private static celexInterface cis;
	private static Hashtable celexsyndb = new Hashtable<String, String>(100000);
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//Removed the code here.
	}
	
	/**
	 * Takes a text string. Returns a Comma seperated string of Celex based Syntactic-Feature values for that Text Document.
	 * Feature string order: in the order in which feature values are defined below in this method..which is the order in which they exist in CelexDB
	 * @param filecontent - String containing text for which features need to be extracted.
	 * @param filepath - is just a pathvariable to keep track. Can be anything.
	 * @return all the feature values as a comma seperated string.
	 * @throws Exception
	 */
	
	public static String getSynFeatures(String filecontent, String filepath) throws Exception
	{
		String[] temp = null;
		try{
			temp= met.tagString(filecontent).split(" "); 
		}
		catch(Exception e)
		{
			System.out.println("Exception while tagging." + e.toString() + "   " + filepath);
			System.exit(1);
		}

		String tempStringofInfo = ""; //Temporary string that is used to extract Values from the Syntactic Dictionary.
		
		//Declare all the variables to calculate feature values (34 in All, for now)
		//Basics 
		double numSentences = 0.0;
		double numWordsConsidered = 0.0;
		double numWordsNotConsidered = 0.0;
		
		//Word Classes - POS - We can use either Celex ClassNums or POS information. I will use POS information as of now - Sowmya, 7thNov2012
		//Field-1 in Loaded hashtable
		int numNouns = 0; //Wordclass ClassNum=1
		int numProperNouns = 0; //No seperate class in CelexDB
		int numAdjective = 0; //Wordclass ClassNum=2
		int numVerb = 0; //ClassNum=4
		int numArticle = 0; //ClassNum=5
		int numPronoun = 0; //ClassNum=6
		int numAdverb = 0; //ClassNum=7
		int numPreposition = 0; //ClassNum=8
		int numConjunction = 0; //ClassNum=9
		int numInterjection = 0; //ClassNum=10
		
		//Noun features - 
		int numCountableN = 0; //Y or N values. ++ if Y. Field-2 in loaded hashtable.
		int numUncountableN = 0; //Y or N values. ++ if Y. Field-3 in loaded hashtable.
		int numSingularOnlyN = 0; //Y or N values. ++ if Y. Field-4 in loaded hashtable.
		int numPluralOnlyN = 0; //Y or N values. ++ if Y. Field-5 in loaded hashtable.
		int numGroupCountableN = 0; //Y or N values. ++ if Y. Field-6 in loaded hashtable.
		int numGroupUncountableN = 0; //Y or N values. ++ if Y. Field-7 in loaded hashtable.
		int numAttrNoun = 0; //Y or N values. ++ if Y. Field-8 in loaded hashtable.
		int numPostPosNoun = 0; //Y or N values. ++ if Y. Field-9 in loaded hashtable.
		int numVocN = 0; ////Y or N values. ++ if Y. Field-10 in loaded hashtable.
		int numExprNoun = 0; //Y or N values. ++ if Y. Field-12 in loaded hashtable.
		
		//Verb Features - 
		int numTransVerb = 0; //Y or N values. ++ if Y. Field-13 in loaded hashtable.
		int numTransCompVerb = 0; //Y or N values. ++ if Y. Field-14 in loaded hashtable. Transitive plus complementation
		int numInTransVerb = 0; //Y or N values. ++ if Y. Field-15 in loaded hashtable.
		int numDiTransVerb = 0; //Y or N values. ++ if Y. Field-16 in loaded hashtable.
		int numLinkingVerb = 0; //Y or N values. ++ if Y. Field-17 in loaded hashtable.
		int numPhrasalVerb = 0; //Y or N values. ++ if Y. Field-18 in loaded hashtable.
		int numPrepVerb = 0; //Y or N values. ++ if Y. Field-19 in loaded hashtable.
		int numPhrPrepVerb = 0; //Y or N values. ++ if Y. Field-20 in loaded hashtable.
		int numExprVerb = 0; //Y or N values. ++ if Y. Field-21 in loaded hashtable.
		
		//Adjective Features -
		int numOrdAdj = 0; //Y or N values. ++ if Y. Field-22 in loaded hashtable. Ordinary ADJ
		int numAttrAdj = 0; //Y or N values. ++ if Y. Field-23 in loaded hashtable. Attributive ADJ
		int numPredAdj = 0; //Y or N values. ++ if Y. Field-24 in loaded hashtable. Predicative ADJ
		int numPostPosAdj = 0; //Y or N values. ++ if Y. Field-25 in loaded hashtable. PostPositive ADJ
		int numExprAdj = 0; //Y or N values. ++ if Y. Field-26 in loaded hashtable. Expressive ADJ
		
		//Adverb Features-
		int numOrdAdv = 0; //Y or N values. ++ if Y. Field-27 in loaded hashtable.
		int numPredAdv = 0; //Y or N values. ++ if Y. Field-28 in loaded hashtable. Predicative ADV
		int numPostPosAdv = 0; //Y or N values. ++ if Y. Field-29 in loaded hashtable. Postpositive Adv
		int numCombAdv = 0; //Y or N values. ++ if Y. Field-30 in loaded hashtable. Combinatory Adv
		int numExprAdv = 0; //Y or N values. ++ if Y. Field-31 in loaded hashtable. Expressive ADV
		
		//Pronoun Features
		int numPersPron = 0; //Y or N values. ++ if Y. Field-35 in loaded hashtable.
		int numDemonsPron = 0; //Y or N values. ++ if Y. Field-36 in loaded hashtable.
		int numPossPron = 0; //Y or N values. ++ if Y. Field-37 in loaded hashtable.
		int numReflPron = 0; //Y or N values. ++ if Y. Field-38 in loaded hashtable.
		int numWhPron = 0; //Y or N values. ++ if Y. Field-39 in loaded hashtable.
		int numDetPron = 0; //Y or N values. ++ if Y. Field-40 in loaded hashtable.
		int numPronPron = 0; //Y or N values. ++ if Y. Field-41 in loaded hashtable. Pronominal PRON
		int numExpPron = 0; //Y or N values. ++ if Y. Field-42 in loaded hashtable.
		
		//Conjunction Features
		int numCoordConj = 0; //Y or N values. ++ if Y. Field-43 in loaded hashtable.
		int numSubConj = 0; //Y or N values. ++ if Y. Field-44 in loaded hashtable.
		
		
		for(String wordtagpair : temp)
		{
			String[] temppair = wordtagpair.split("_");
			String generaltag = "NOTAG"; //This is to give to the Lemmatizer.
			String word = temppair[0];
			String tag = temppair[1];
			String lemma = " ";
			String morpheme;
			
			if(tag.equals("VB") || tag.equals("VBD") || tag.equals("VBG") || tag.equals("VBN") || tag.equals("VBP") || tag.equals("VBZ"))
			{
       			generaltag = "VERB";
       			numVerb++;
			}
			
			else if(tag.equals("JJ") || tag.equals("JJR") || tag.equals("JJS"))
			{
				generaltag = "ADJECTIVE";
				numAdjective++;
			}
			else if(tag.equals("RB") || tag.equals("RBR") || tag.equals("RBS"))
			{
				generaltag = "ADVERB";
				numAdverb++;
			}
			else if(tag.equals("PRP") || tag.equals("PRP$") || tag.equals("WP") || tag.equals("WP$"))
			{
				//generaltag = "PRONOUN";
				numPronoun++;
			} 
			
			else if(tag.equals("NN") || tag.equals("NNS"))
			{
				generaltag = "NOUN";
				numNouns++;
			}	
			
			else if(tag.equals("NNP") || tag.equals("NNPS"))
			{
				numProperNouns++;
			}
			
			else if(tag.equals("UH"))
			{
				numInterjection++;
			}
			
			else if(tag.equals("CC"))
			{
				numConjunction++;
			}
			
			else if(tag.equals("IN"))
			{
				numPreposition++;
			}
			
			else if(tag.equals("DT"))
			{
				numArticle++;
			}
			
			else if(tag.equals("."))
			{
				numSentences++;
			}
			
			 if(!generaltag.equals("NOTAG"))
			 {
					
				if(stemmer.findStems(word, POS.valueOf(generaltag)).size() > 0)
				{
				 lemma = stemmer.findStems(word, POS.valueOf(generaltag)).get(0);	
				}
				else
				{
					lemma = word;
				}
			
				//If the Lemma exists in MorphemeDb
				if(celexsyndb.containsKey(lemma.toString()))
				{				
					morpheme = lemma;
				}
				
				//Check IF the word directly exists in the morphemedb
				else if(celexsyndb.containsKey(word))
				{
					morpheme = word;
					
				}
				
				//Just ignore this process for this word!
				else
				{
					morpheme = " ";
					numWordsNotConsidered++;
				}
				
				
				if(!morpheme.equals(" "))
				{
					numWordsConsidered++;
					//Write all the extraction code here.
					tempStringofInfo = celexsyndb.get(morpheme).toString();	
					String[] features = tempStringofInfo.split("\\|");
					
					//Now, extract all features.
					//Noun features - Fields 2,3,4,5,6,7,8,9,10,12
					if(features[2].equals("Y"))
					{
						numCountableN++;
					}
					if(features[3].equals("Y"))
					{
						numUncountableN++;
					}
					if(features[4].equals("Y"))
					{
						numSingularOnlyN++;
					}
					if(features[5].equals("Y"))
					{
						numPluralOnlyN++;
					}
					if(features[6].equals("Y"))
					{
						numGroupCountableN++;
					}
					if(features[7].equals("Y"))
					{
						numGroupUncountableN++;
					}
					if(features[8].equals("Y"))
					{
						numAttrNoun++;
					}
					if(features[9].equals("Y"))
					{
						numPostPosNoun++;
					}
					if(features[10].equals("Y"))
					{
						numVocN++;
					}
					if(features[12].equals("Y"))
					{
						numExprNoun++;
					}
					//Verb features - 13,14,15,16,17,18,19,20,21
					if(features[13].equals("Y"))
					{
						numTransVerb++;
					}
					if(features[14].equals("Y"))
					{
						numTransCompVerb++;
					}
					if(features[15].equals("Y"))
					{
						numInTransVerb++;
					}
					if(features[16].equals("Y"))
					{
						numDiTransVerb++;
					}
					if(features[17].equals("Y"))
					{
						numLinkingVerb++;
					}
					if(features[18].equals("Y"))
					{
						numPhrasalVerb++;
					}
					if(features[19].equals("Y"))
					{
						numPrepVerb++;
					}
					if(features[20].equals("Y"))
					{
						numPhrPrepVerb++;
					}
					if(features[21].equals("Y"))
					{
						numExprVerb++;
					}
					//Adjective Features. 22,23,24,25,26
					if(features[22].equals("Y"))
					{
						numOrdAdj++;
					}
					if(features[23].equals("Y"))
					{
						numAttrAdj++;
					}
					if(features[24].equals("Y"))
					{
						numPredAdj++;
					}
					if(features[25].equals("Y"))
					{
						numPostPosAdj++;
					}
					if(features[26].equals("Y"))
					{
						numExprAdj++;
					}
					//Adverb Features. 27,28,29,30,31.
					if(features[27].equals("Y"))
					{
						numOrdAdv++;
					}
					if(features[28].equals("Y"))
					{
						numPredAdv++;
					}
					if(features[29].equals("Y"))
					{
						numPostPosAdv++;
					}
					if(features[30].equals("Y"))
					{
						numCombAdv++;
					}
					if(features[31].equals("Y"))
					{
						numExprAdv++;
					}
					//Pronoun features. 35,36,37,38,39,40,41,42
					if(features[35].equals("Y"))
					{
						numPersPron++;
					}
					if(features[36].equals("Y"))
					{
						numDemonsPron++;
					}
					if(features[37].equals("Y"))
					{
						numPossPron++;
					}
					if(features[38].equals("Y"))
					{
						numReflPron++;
					}
					if(features[39].equals("Y"))
					{
						numWhPron++;
					}
					if(features[40].equals("Y"))
					{
						numDetPron++;
					}
					if(features[41].equals("Y"))
					{
						numPronPron++;
					}
					if(features[42].equals("Y"))
					{
						numExpPron++;
					}
					//Conjunctioin features: 43,44
					if(features[43].equals("Y"))
					{
						numCoordConj++;
					}
					if(features[44].equals("Y"))
					{
						numSubConj++;
					}
				} //End of if for !morpheme.equals(" ")
			
			 } //End of if for generaltag.equals("NOTAG") 
		
			 else
			 {
				 numWordsNotConsidered++;
			 }
		} // End of for for(String wordtagpair : temp)
		
		//Now Collect all info gathered sofar, in to a string and return it to the main program!
		double totalwords = numWordsConsidered + numWordsNotConsidered;
		
		String result = "None";
		
		if(numSentences == 0 && totalwords > 0)
		{
			numSentences = 1;
		}
		
		if(numSentences > 0)
		{
		 result = numSentences + "," + restrict2TwoDecimals(numWordsConsidered/totalwords) + "," + restrict2TwoDecimals(numWordsNotConsidered/totalwords)
						+","+restrict2TwoDecimals(numNouns/numSentences)+","+restrict2TwoDecimals(numProperNouns/numSentences)
						+","+restrict2TwoDecimals(numAdjective/numSentences)+","+restrict2TwoDecimals(numVerb/numSentences)
						+","+restrict2TwoDecimals(numArticle/numSentences)+","+restrict2TwoDecimals(numPronoun/numSentences)
						+","+restrict2TwoDecimals(numAdverb/numSentences)+","+restrict2TwoDecimals(numPreposition/numSentences)
						+","+restrict2TwoDecimals(numConjunction/numSentences)+","+restrict2TwoDecimals(numInterjection/numSentences)
						+","+restrict2TwoDecimals(numCountableN/numSentences)+","+restrict2TwoDecimals(numUncountableN/numSentences)
						+","+restrict2TwoDecimals(numSingularOnlyN/numSentences)+","+restrict2TwoDecimals(numPluralOnlyN/numSentences)
						+","+restrict2TwoDecimals(numGroupCountableN/numSentences)+","+restrict2TwoDecimals(numGroupUncountableN/numSentences)
						+","+restrict2TwoDecimals(numAttrNoun/numSentences)+","+restrict2TwoDecimals(numPostPosNoun/numSentences)
						+","+restrict2TwoDecimals(numVocN/numSentences)+","+restrict2TwoDecimals(numExprNoun/numSentences)
						+","+restrict2TwoDecimals(numTransVerb/numSentences)+","+restrict2TwoDecimals(numTransCompVerb/numSentences)
						+","+restrict2TwoDecimals(numInTransVerb/numSentences)+","+restrict2TwoDecimals(numDiTransVerb/numSentences)
						+","+restrict2TwoDecimals(numLinkingVerb/numSentences)+","+restrict2TwoDecimals(numPhrasalVerb/numSentences)
						+","+restrict2TwoDecimals(numPrepVerb/numSentences)+","+restrict2TwoDecimals(numPhrPrepVerb/numSentences)
						+","+restrict2TwoDecimals(numExprVerb/numSentences)+","+restrict2TwoDecimals(numOrdAdj/numSentences)
						+","+restrict2TwoDecimals(numAttrAdj/numSentences)+","+restrict2TwoDecimals(numPredAdj/numSentences)
						+","+restrict2TwoDecimals(numPostPosAdj/numSentences)+","+restrict2TwoDecimals(numExprAdj/numSentences)
						+","+restrict2TwoDecimals(numOrdAdv/numSentences)+","+restrict2TwoDecimals(numPredAdv/numSentences)
						+","+restrict2TwoDecimals(numPostPosAdv/numSentences)+","+restrict2TwoDecimals(numCombAdv/numSentences)
						+","+restrict2TwoDecimals(numExprAdv/numSentences)+","+restrict2TwoDecimals(numPersPron/numSentences)
						+","+restrict2TwoDecimals(numDemonsPron/numSentences)+","+restrict2TwoDecimals(numPossPron/numSentences)
						+","+restrict2TwoDecimals(numReflPron/numSentences)+","+restrict2TwoDecimals(numWhPron/numSentences)
						+","+restrict2TwoDecimals(numDetPron/numSentences)+","+restrict2TwoDecimals(numPronPron/numSentences)
						+","+restrict2TwoDecimals(numExpPron/numSentences)+","+restrict2TwoDecimals(numCoordConj/numSentences)
						+","+restrict2TwoDecimals(numSubConj/numSentences);
					
					
		}
		return result;
	}
	
	/**
	 * Initialization code for this class. Loads CelexDB, sets up WN to do lemmatization etc.
	 * @throws Exception
	 */
	public static void initialize() throws Exception
	{
		//Tagger initialization (To give to Lemmatizer)
		met = new MaxentTagger("db/wsj-0-18-left3words.tagger"); //Path to POS Tagger Model.
		
		//Wordnet Initialization, for Lemmatizer purposes (To get Lemma and search in celexdb)
		String wnpath = "db/dict"; //Path for WN Dictionary
		URL url = null;
		try{ url = new URL("file", null, wnpath); } 
		catch(MalformedURLException e){ e.printStackTrace(); }
		if(url == null) return;
		dict = new Dictionary(url);
		dict.open();
		stemmer = new WordnetStemmer(dict);
		System.out.println("Setup wordnet only to use the lemmatizer part of it for now! ");
			
		//Load Celex Syntactic features Data
		cis = new celexInterface();
		String celexSynFilePath =  "db/esl.cd"; //Path for Celex Syntactic Features-Lemma DB.
		celexsyndb =  cis.createHashTable(celexSynFilePath);
		System.out.println("Loaded Celex DB of size :"+celexsyndb.size() + " word lemmas");
		
	}
	
	/*
	 * Okay, I don't know why I have this so many times. But, no harm.
	 */
	private static double restrict2TwoDecimals(double D)
	{
		int temp = (int)(D*1000);
		return (double)(temp/1000.0);
	}
}
