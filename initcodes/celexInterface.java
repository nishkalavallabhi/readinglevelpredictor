package initcodes;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Hashtable;

/**
 * 
 */

/**
 * @author svajjala
 * Loads Celex Database from the disk, into a Hashtable, indexed by the lemma.
 * More on celex at: http://www.ldc.upenn.edu/Catalog/readme_files/celex.readme.html
 */
public class celexInterface {
	
	
	public static void main(String args[]) throws Exception
	{
		//celexInterface ci = new celexInterface();
	   //ci.createHashTable(celexMorphFilePath);
 		//System.out.println(morphemedb.size());
 		//System.out.println(morphemedb.get("abandon"));
		
		//This is some general trial code.
 	}
	
	/**
	 * Creates a Hashtable containing lemmas in CelexDb and their properties.
	 * @param celexMorphFilePath : the path for celex morphological db or celex syntactic db
	 * @return A hashtable that contains lemma,properties as key and value pairs.
	 * @throws Exception
	 */
	public Hashtable<String, String> createHashTable(String celexFilePath) throws Exception
	{
		Hashtable celexdb = new Hashtable<String, String>(100000);
		BufferedReader br = new BufferedReader(new FileReader(celexFilePath));
		String dummy = "";
		//To generally understand why all this is being done, look at the celex formats from db/eml.cd for Morphological properties and db/esl.cde for Syntactic properties.
		while((dummy=br.readLine())!=null)
		{
			//Thats majorly because I felt irritated with those escape characters. No other reason.
			String[] tempMorphemeDbElement = dummy.replace("\\","|").split("\\|",4);
			
			String tempMorpheme = tempMorphemeDbElement[1];
			int tempMorphemeFreq = Integer.parseInt(tempMorphemeDbElement[2]);
			
			if(celexdb.containsKey(tempMorpheme))
			{
				int morphemeFreqFromTable = Integer.parseInt(celexdb.get(tempMorpheme).toString().split("\\|")[0]);
			    if(tempMorphemeFreq > morphemeFreqFromTable)
				{
			    	celexdb.put(tempMorpheme, tempMorphemeDbElement[2]+"|"+tempMorphemeDbElement[3]);
				}
			}
			else
			{
				celexdb.put(tempMorpheme, tempMorphemeDbElement[2]+"|"+tempMorphemeDbElement[3]);
			}
		}
		br.close();
		return celexdb;
	}

}
